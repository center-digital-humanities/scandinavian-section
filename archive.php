<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (is_archive() || is_category()) { ?>
					<h1 class="page-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } elseif (is_tag()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Articles Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
					</h1>
					<?php } elseif (is_author()) {
						global $post;
						$author_id = $post->post_author;
					?>
					<h1 class="page-title">
						<span><?php _e( 'Articles By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
					</h1>
					<?php } elseif (is_day()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
					</h1>
					<?php } elseif (is_month()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
					</h1>
					<?php } elseif (is_year()) { ?>
					<h1 class="page-title">
						<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
					</h1>
					<?php } ?>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '' . $category_description . '' );
					?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<?php if (is_category()) { ?><span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
						<?php } ?>
						<?php if (is_tax( 'research_cat' )) { ?>
						<?php $research_link = get_field('research_link'); ?>
						<span class="researcher">
						<strong>Researcher: </strong>
							<?php $post_object = get_field('researcher');
							if( $post_object ): 
								$post = $post_object;
								setup_postdata( $post ); 
							?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php } ?>
						<section class="entry-content cf">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>