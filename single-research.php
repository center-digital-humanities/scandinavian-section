<?php get_header(); ?>
			<div class="content">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $research_link = get_field('research_link'); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php  if ( has_post_thumbnail() ) { ?>
							<?php if( $research_link ) { ?><a href="<?php echo $research_link ?>"><?php } ?><?php the_post_thumbnail( 'content-width' ); ?><?php if( $research_link ) { ?></a><?php } ?>
							<?php } ?>
							<span class="researcher">
							<strong>Researcher: </strong>
								<?php $post_object = get_field('researcher');
								if( $post_object ): 
									$post = $post_object;
									setup_postdata( $post ); 
								?>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<?php wp_reset_postdata(); ?>
								<?php endif; ?>
							</span>
							<?php the_content(); ?>
							<?php if( $research_link ) { ?><a href="<?php echo $research_link ?>" class="btn">Visit Website <span class="hidden">for <?php the_title(); ?></span></a><?php } ?>
						</section>
					</article>
					<?php wp_reset_postdata(); ?>
					<?php endwhile; else : ?>
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>